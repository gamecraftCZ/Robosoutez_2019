package cz.pusici.Algorithms;

import cz.pusici.Algorithms.Utils.PathFinder;
import cz.pusici.HighLevelControl.Direction;
import cz.pusici.HighLevelControl.Position;

import java.util.ArrayList;
import java.util.List;

import static cz.pusici.Config.BUTTON_STEPPING_DEBUG;
import static cz.pusici.Config.IS_DEBUG;
import static cz.pusici.HighLevelControl.Controls.getControls;
import static cz.pusici.HighLevelControl.Direction.*;
import static cz.pusici.Utils.waitForKey;


/**
 * WORKS with virtual field.
 */
public class FindHighest extends AlgUtils implements Algorithm {

    private boolean lookAround;
    private boolean rideBeforeDecide;
    private boolean rightFirst;
    private boolean canFall2Blocks;

    private PathFinder pathFinder;


    public FindHighest(boolean lookAround, boolean rideBeforeDecide, boolean rightFirst,
                       boolean canFall2Blocks) {
        super();
        this.lookAround = lookAround;
        this.rideBeforeDecide = rideBeforeDecide;
        this.rightFirst = rightFirst;
        this.canFall2Blocks = canFall2Blocks;

        initializePathFinder();
    }

    private void initializePathFinder() {
        int maxFall = 1;
        if (canFall2Blocks) maxFall = 2;
        pathFinder = new PathFinder(field, maxFall, rightFirst);
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void start() {
        System.out.println("ALG: BackToHighest");
        printFieldWithPosition();

        // Continue normally
        while (true) {
            testUltrasonicSmart();
            markCurrentBlockAsVisited();
            printFieldWithPosition();
            if (BUTTON_STEPPING_DEBUG) waitForKey();

            List<Position> doNotInclude = new ArrayList<Position>();

            Position target = findPossibleHighestBlockPosition(pathFinder, rightFirst);
            if (lookAround) {
                if (target.z < field.highestHigh) {
                    lookAroundSmart(rightFirst);
                }
            }
            target = findPossibleHighestBlockPosition(pathFinder, rightFirst);

            PathFinder.Path path = pathFinder.findPath(target);

            while (path == null) {
                if (rideBeforeDecide && lookAround) {
                    lookAroundSmart(rightFirst);
                }

                System.out.println("No path to block " + target + ", trying another.");
                doNotInclude.add(target);
                target = field.getClosestUnvisitedBlock(pathFinder, doNotInclude).getPosition();
                path = pathFinder.findPath(target);
            }


            if (IS_DEBUG) System.out.println("                      TARGET: " + target);
            Direction bestDirectionToTheEnd = path.direction;
            if (IS_DEBUG) System.out.println("Best direction to the target: " + bestDirectionToTheEnd);
            switch (getControls().getDirection()) {
                case UP:
                    if (bestDirectionToTheEnd == UP) forwardOrClimbOrFall();
                    else if (bestDirectionToTheEnd == LEFT) getControls().rotateLeft();
                    else if (bestDirectionToTheEnd == RIGHT) getControls().rotateRight();
                    else if (bestDirectionToTheEnd == DOWN) getControls().rotateRight();
                    break;
                case LEFT:
                    if (bestDirectionToTheEnd == UP) getControls().rotateRight();
                    else if (bestDirectionToTheEnd == LEFT) forwardOrClimbOrFall();
                    else if (bestDirectionToTheEnd == RIGHT) getControls().rotateRight();
                    else if (bestDirectionToTheEnd == DOWN) getControls().rotateLeft();
                    break;
                case RIGHT:
                    if (bestDirectionToTheEnd == UP) getControls().rotateLeft();
                    else if (bestDirectionToTheEnd == LEFT) getControls().rotateLeft();
                    else if (bestDirectionToTheEnd == RIGHT) forwardOrClimbOrFall();
                    else if (bestDirectionToTheEnd == DOWN) getControls().rotateRight();
                    break;
                case DOWN:
                    if (bestDirectionToTheEnd == UP) getControls().rotateLeft();
                    else if (bestDirectionToTheEnd == LEFT) getControls().rotateRight();
                    else if (bestDirectionToTheEnd == RIGHT) getControls().rotateLeft();
                    else if (bestDirectionToTheEnd == DOWN) forwardOrClimbOrFall();
            }

            printFieldWithPosition();
//            if (IS_VIRTUAL) sleep(2000);
        }
    }

}
