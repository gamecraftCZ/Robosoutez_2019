package cz.pusici.Algorithms.Utils;

import cz.pusici.HighLevelControl.Position;

import java.util.ArrayList;
import java.util.List;

import static cz.pusici.Utils.randomInt;


public class Field {
    private static Field instance;

    public final Block[][] field;
    public final int sizeX;
    public final int sizeY;

    public int highestFound = 0;
    public int highestHigh;


    /**
     * Creates empty field.
     * @param sizeX Size of the playing field on X axis.
     * @param sizeY Size of the playing field on Y axis.
     */
    public Field(int sizeX, int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        Block[][] newField = new Block[sizeY+2][sizeX+2];

        // Fill all with 9 high walls.
        for (int yAxis = 0; yAxis < sizeY+2; yAxis++) {
            for (int xAxis = 0; xAxis < sizeX+2; xAxis++) {
                newField[yAxis][xAxis] = new Block(new Position(xAxis, yAxis, 9)).setExact().setVisited();
            }
        }

        // Fill play area with 0 high walls.
        for (int yAxis = 1; yAxis <= sizeY; yAxis++) {
            for (int xAxis = 1; xAxis < sizeX+1; xAxis++) {
                newField[yAxis][xAxis] = new Block(new Position(xAxis, yAxis, 0));
            }
        }

        // Set start and finish blocks to be exact and visited
        for (int xAxis = 1; xAxis < sizeX+1; xAxis++) {
            newField[1][xAxis].setExact().setVisited();
            newField[sizeY][xAxis].setExact().setVisited();
        }

        field = newField;
    }

    public Block getOnPosition(Position position) {
        if (position.x > sizeX+1 || position.y > sizeY+1) return null;
        if (position.x < 0 || position.y < 0) return null;
        return field[position.y][position.x];
    }

    public void resetBlocksPathFindingAttributes() {
        for (Block[] yBlocks : field) {
            for (Block block : yBlocks) {
                block.pathFindingVisited = false;
                block.pathFindingOriginalDirection = null;
                block.pathFindingLength = 999;
            }
        }
    }

    public Block getRandomUnvisitedBlock() {
        List<Block> unvisitedBlocks = getUnvisitedBlocks();
        int randomBlockIndex = randomInt(0, unvisitedBlocks.size());
        return unvisitedBlocks.get(randomBlockIndex);
    }

    public Block getClosestUnvisitedBlock(PathFinder pathFinder, List<Position> doNotInclude) {
        pathFinder.findPath(new Position(0, 0, 9));
        List<Block> unvisitedBlocks = getUnvisitedBlocks();

        Block closestBlock = unvisitedBlocks.get(0);
        for (Block block : unvisitedBlocks) {
            if (block.pathFindingLength < closestBlock.pathFindingLength) {
                boolean exclude = false;
                for (Position excludedPosition : doNotInclude) {
                    if (block.getPosition().equals(excludedPosition)) {
                        exclude = true;
                    }
                }
                if (!exclude) closestBlock = block;
            }
        }

        return closestBlock;
    }

    public List<Block> getUnvisitedBlocks() {
        List<Block> unvisitedBlocks = new ArrayList<Block>();

        for (Block[] yBlocks : field) {
            for (Block block : yBlocks) {
                if (block.isNotVisited()) unvisitedBlocks.add(block);
            }
        }

        return unvisitedBlocks;
    }

    
    public String toString() {
        StringBuilder fieldString = new StringBuilder();
        if (true) { //IS_VIRTUAL) {
            // Longer version for computer console
            for (int yAxis = field.length - 1; yAxis >= 0; yAxis--) {
                fieldString.append(yAxis).append(".  ");
                for (int xAxis = 0; xAxis < field[0].length; xAxis++) {
                    fieldString.append(field[yAxis][xAxis].getPosition().z).append("  ");
                }
                fieldString.append("\n");
            }
            fieldString.append("y x ");
            for (int xAxis = 0; xAxis < field[0].length; xAxis++) {
                fieldString.append(xAxis).append(".  ");
            }
        } else {
            // Shorter version for NXT display
            for (int xAxis = 1; xAxis < field[0].length - 1; xAxis++) {
                for (int yAxis = field.length - 2; yAxis >= 1; yAxis--) {
                    fieldString.append(field[yAxis][xAxis].getPosition().z).append(" ");
                }
                fieldString.append("\n");
            }
        }
        return fieldString.toString();
    }

    public String toString(Position highlight) {
        StringBuilder fieldString = new StringBuilder();
        if (true) { //IS_VIRTUAL) {
            // Longer version for computer console
            for (int yAxis = field.length - 1; yAxis >= 0; yAxis--) {
                fieldString.append(yAxis).append(".  ");
                for (int xAxis = 0; xAxis < field[0].length; xAxis++) {
                    fieldString.append(field[yAxis][xAxis].toString());
                    if (highlight.x == xAxis && highlight.y == yAxis) fieldString.append("X ");
                    else fieldString.append("  ");
                }
                fieldString.append("\n");
            }
            fieldString.append("y x ");
            for (int xAxis = 0; xAxis < field[0].length; xAxis++) {
                fieldString.append(xAxis).append(".   ");
            }
        } else {
            // Shorter version for NXT display
            for (int xAxis = 1; xAxis < field[0].length - 1; xAxis++) {
                for (int yAxis = 1; yAxis < field.length - 1; yAxis++) {
                    if (highlight.y == yAxis) {
                        if (highlight.x == xAxis) fieldString.append(field[yAxis][xAxis].getPosition().z).append("X ");
                        else fieldString.append(field[yAxis][xAxis].getPosition().z).append("  ");
                    }
                    else fieldString.append(field[yAxis][xAxis].getPosition().z).append("");
                }
                fieldString.append("\n");
            }
        }
        return fieldString.toString();
    }



    public static Field getField() {
        return instance;
    }

    public static Field createField(int highestHigh) {
        instance = new Field(6, 9);
        instance.highestHigh = highestHigh;
        return instance;
    }

}
