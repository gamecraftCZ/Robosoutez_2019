package cz.pusici.Algorithms.Utils;

import cz.pusici.HighLevelControl.Direction;
import cz.pusici.HighLevelControl.Position;

import java.util.ArrayList;
import java.util.List;

import static cz.pusici.HighLevelControl.Controls.getControls;
import static cz.pusici.HighLevelControl.Direction.*;

public class PathFinder {

    private boolean customIsTargetChecker;
    private IsTarget isTargetChecker;
    private IsTarget oldIsTargetChecker;
    private Field field;
    public int maxFall;
    public boolean rightFirst;

    /**
     * Use this constructor to use your own checker for checking if target was found.
     * @param field Field to find path on.
     * @param isTargetChecker Custom isTarget checker to check if desired finish was found.
     */
    public PathFinder(Field field, int maxFall, boolean rightFirst, IsTarget isTargetChecker) {
        this.field = field;
        this.isTargetChecker = isTargetChecker;
        this.maxFall = maxFall;
        this.rightFirst = rightFirst;
        customIsTargetChecker = true;
    }

    /**
     * Use this constructor for finding using Position.
     * @param field Field to find path on.
     */
    public PathFinder(Field field, int maxFall, boolean rightFirst) {
        this.field = field;
        this.maxFall = maxFall;
        this.rightFirst = rightFirst;
    }


    private List<Block> pathFindingBlocks = new ArrayList<Block>();

    /**
     * @return Returns Direction of the best path to the end.
     */
    public Path findPath() {
        if (!customIsTargetChecker) {
            throw new IllegalStateException("Can't use PathFinding.findPath() without parameters " +
                    "when not using custom isTargetChecker.");
        }

        Path path = findPathPrivate();
        return path;
    }
    public Path findPath(final Position target) {
        oldIsTargetChecker = isTargetChecker;
        createTargetChecker(target);

        Path path = findPathPrivate();

        isTargetChecker = oldIsTargetChecker;
        return path;
    }
    private void createTargetChecker(final Position target) {
        this.isTargetChecker = new IsTarget() {
            @Override
            public boolean isTarget(Position position) {
                return target.equals(position);
            }
        };
    }
    private Path findPathPrivate() {
        field.resetBlocksPathFindingAttributes();
        Block startBlock = field.getOnPosition(getControls().getPosition());

        if (prepareInitialBlockForPathFinding(startBlock, UP)) return new Path(UP, 1);
        if (prepareInitialBlockForPathFinding(startBlock, DOWN)) return new Path(DOWN, 1);
        if (rightFirst) {
            if (prepareInitialBlockForPathFinding(startBlock, RIGHT)) return new Path(RIGHT, 1);
            if (prepareInitialBlockForPathFinding(startBlock, LEFT)) return new Path(LEFT, 1);
        } else {
            if (prepareInitialBlockForPathFinding(startBlock, LEFT)) return new Path(LEFT, 1);
            if (prepareInitialBlockForPathFinding(startBlock, RIGHT)) return new Path(RIGHT, 1);
        }

        try {
            while (pathFindingBlocks.size() > 0) {
                Block block = pathFindingBlocks.get(0);
                if (pathFindingTryBlock(block, UP)) return new Path(block.pathFindingOriginalDirection, block.pathFindingLength);
                if (pathFindingTryBlock(block, DOWN)) return new Path(block.pathFindingOriginalDirection, block.pathFindingLength);
                if (pathFindingTryBlock(block, LEFT)) return new Path(block.pathFindingOriginalDirection, block.pathFindingLength);
                if (pathFindingTryBlock(block, RIGHT)) return new Path(block.pathFindingOriginalDirection, block.pathFindingLength);
                pathFindingBlocks.remove(0);
            }

            System.out.println("ERROR - NO Path found!");
            return null;
        } finally {
            pathFindingBlocks.clear();
        }
    }

    private boolean prepareInitialBlockForPathFinding(Block startBlock, Direction direction) {
        int originalPathFindingBlocksSize = pathFindingBlocks.size();
        if (pathFindingTryBlock(startBlock, direction)) return true;
        if (originalPathFindingBlocksSize < pathFindingBlocks.size()) {
            Block block = pathFindingBlocks.get(originalPathFindingBlocksSize);
            block.pathFindingOriginalDirection = direction;
            block.pathFindingLength = 1;
        }
        return false;
    }
    // Returns true if next block is end of field.
    private boolean pathFindingTryBlock(Block currentBlock, Direction direction) {
        Block newBlock = field.getOnPosition(currentBlock.getPosition().atDirection(direction));
        if (newBlock == null) return false;
        if (newBlock.pathFindingVisited) return false;

        if (newBlock.isMaybeHigher()) {
            if (newBlock.getHeight() > currentBlock.getHeight() + 1) return false;
            addBlockToPathFindingUsed(newBlock, currentBlock);

        } else if(newBlock.isMaybeLower()) {
            if (newBlock.getHeight() < currentBlock.getHeight() - maxFall) return false;
            addBlockToPathFindingUsed(newBlock, currentBlock);

        } else {
            if (newBlock.getHeight() > currentBlock.getHeight() + 1) return false;
            if (newBlock.getHeight() < currentBlock.getHeight() - maxFall) return false;
            addBlockToPathFindingUsed(newBlock, currentBlock);

        }

        return isTargetChecker.isTarget(newBlock.getPosition());
    }
    private void addBlockToPathFindingUsed(Block block, Block previousBlock) {
        block.pathFindingVisited = true;
        block.pathFindingOriginalDirection = previousBlock.pathFindingOriginalDirection;
        block.pathFindingLength = previousBlock.pathFindingLength + 1;
        pathFindingBlocks.add(block);
    }



    public interface IsTarget {
        boolean isTarget(Position position);
    }

    public class Path {
        public Direction direction;
        public int length;

        public Path(Direction direction, int length) {
            this.direction = direction;
            this.length = length;
        }
    }


}
