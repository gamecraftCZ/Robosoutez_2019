package cz.pusici.Algorithms.Utils;

import cz.pusici.HighLevelControl.Direction;
import cz.pusici.HighLevelControl.Position;

public class Block {

    private final Position position;
    private boolean maybeHigher = true;
    private boolean maybeLower;
    private boolean exact;

    public boolean visited = false;  // Did I already visited this block?

    public boolean pathFindingVisited = false;  // For path finding algorithm
    public Direction pathFindingOriginalDirection = null;
    public int pathFindingLength = 0;


    public Block(Position position) {
        this.position = position;
    }

    public Position getPosition() {
        return position;
    }

    public boolean isMaybeHigher() {
        return maybeHigher;
    }
    public boolean isNotMaybeHigher() {
        return !maybeHigher;
    }
    public Block setMaybeHigher() {
        maybeHigher = true;
        maybeLower = false;
        exact = false;
        return this;
    }

    public boolean isMaybeLower() {
        return maybeLower;
    }
    public boolean isNotMaybeLower() {
        return !maybeLower;
    }
    public Block setMaybeLower() {
        maybeHigher = false;
        maybeLower = true;
        exact = false;
        if (position.z == 0) setExact();
        return this;
    }

    public boolean isExact() {
        return exact;
    }
    public boolean isNotExact() {
        return !exact;
    }
    public Block setExact() {
        maybeHigher = false;
        maybeLower = false;
        exact = true;
        return this;
    }

    public boolean isVisited() {
        return visited;
    }
    public boolean isNotVisited() {
        return !visited;
    }
    public Block setVisited() {
        visited = true;
        return this;
    }


    public byte setHeight(int newHeight) {
        position.z = (byte) newHeight;
        return position.z;
    }
    public byte setHeight(byte newHeight) {
        position.z = newHeight;
        return position.z;
    }

    public byte getHeight() {
        return position.z;
    }



    public int heightDifferenceInDirection(Field field, Direction direction) {
        Block block = field.getOnPosition(position.atDirection(direction));
        return block.getHeight() - this.getHeight();
    }


    @Override
    public String toString() {
        String string = "" + position.z;

        if (visited) string += ".";
        else string += " ";

        if (maybeHigher) string += "+";
        else if (maybeLower) string += "-";
        else if (exact) string += "|";
        else string += " ";

        return string;
    }
}
