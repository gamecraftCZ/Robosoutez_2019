package cz.pusici.Algorithms;

import static cz.pusici.Config.*;
import static cz.pusici.HighLevelControl.Controls.getControls;
import static cz.pusici.LowLevelControl.Motors.getMotors;
import static cz.pusici.LowLevelControl.Sensors.getSensors;
import static cz.pusici.Utils.waitForKey;


/**
 * Does NOT WORK with virtual field!
 */
public class OLD_FastToEnd extends AlgUtils implements Algorithm {

    public static int blocksMove = 8;


    public OLD_FastToEnd() {
        super();
    }


    @SuppressWarnings("Duplicates")
    @Override
    public void start() {
        System.out.println("ALG: FastToEnd");
        printFieldWithPosition();

        while (true) {
            testUltrasonicSmart();  // Takes no time
            printFieldWithPosition();
            if (BUTTON_STEPPING_DEBUG) waitForKey();

            if (getControls().getPosition().z == 0) {
                int originalMovementRotations = getMotors().getMovementRotations();

                getMotors().forwardRotation(BLOCK_MOTOR_ROTATIONS * (blocksMove + 1 - getControls().getPosition().y), true);
                int light = getSensors().frontLight.getLightValue();
//                Position currentPosition = getControls().getRotations().clone();
//                int lastCheckedBlockMoved = 0;
                while (getMotors().isMoving() && light > FRONT_LIGHT_THRESHOLD) {
                    light = getSensors().frontLight.getLightValue();
                    if (IS_DEBUG) System.out.println("LIGHT: " + light);

                    // Check ultrasonic
//                    int blocksMoved = (getMotors().getMovementRotations() - originalMovementRotations + 100) / BLOCK_MOTOR_ROTATIONS;
//                    if (blocksMoved > lastCheckedBlockMoved) {
//                        currentPosition.y = getControls().getRotations().y += blocksMoved;
//                        testUltrasonicSmart(currentPosition, UP);
//                        lastCheckedBlockMoved = blocksMoved;
//                    }
                }
                getMotors().stop();

                // Add blocks moved to position.y
                int blocksMoved = (getMotors().getMovementRotations() - originalMovementRotations + 50) / BLOCK_MOTOR_ROTATIONS;
                System.out.println("Moved: " + blocksMoved);
                for (int i = 0; i < blocksMoved; i++) {
                    getControls().getPosition().y ++;
                    field.getOnPosition(getControls().getPosition()).setVisited().setExact().setHeight(0);
                }
                printFieldWithPosition();

                // Test if climb is needed
                if (light < FRONT_LIGHT_THRESHOLD) {
                    if (IS_DEBUG) System.out.println("Climbing");
                    getMotors().backwardRotation(60);
                    getMotors().stop();
                    getControls().climbForward();
                    field.getOnPosition(getControls().getPosition()).setVisited().setExact().setHeight(getControls().getPosition().z);  // Set block height
                    System.out.println("Climbed");
                    printFieldWithPosition();
                }
            } else {
                forwardOrClimbOrFall();
            }

            printFieldWithPosition();
            if ((blocksMove + 1 - getControls().getPosition().y) <= 0) return;  // End after robot is at the end.
        }
    }

}
