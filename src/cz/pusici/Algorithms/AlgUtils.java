package cz.pusici.Algorithms;

import cz.pusici.Algorithms.Utils.Block;
import cz.pusici.Algorithms.Utils.Field;
import cz.pusici.Algorithms.Utils.PathFinder;
import cz.pusici.HighLevelControl.Direction;
import cz.pusici.HighLevelControl.Position;

import java.util.ArrayList;
import java.util.List;

import static cz.pusici.Algorithms.Utils.Field.getField;
import static cz.pusici.Config.IS_DEBUG;
import static cz.pusici.Config.IS_VIRTUAL;
import static cz.pusici.HighLevelControl.Controls.getControls;

public class AlgUtils {
    Field field = getField();

    void testUltrasonicSmart() {
        Direction direction = getControls().getDirection();
        if (isNotBlockInDirectionExact(direction)) {
            testUltrasonic();
        }
    }

    private void testUltrasonic() {
        Position position = getControls().getPosition();
        Direction direction = getControls().getDirection();

        Position blockPosition = position.clone().atDirection(direction);
        Block block = getField().getOnPosition(blockPosition);

        // Check front block height
        int relativeHeight = getControls().testForward();
        int blockHeight = position.z + relativeHeight;
        if (blockHeight < 0) blockHeight = 0;
        block.setExact().setHeight(blockHeight);
    }

    void forwardOrClimbOrFall() {
        Position position = getControls().getPosition();
        Direction direction = getControls().getDirection();

        Position blockPosition = position.clone().atDirection(direction);
        Block block = field.getOnPosition(blockPosition);

        if (block.getHeight() > position.z) getControls().climbForward();

        else if (block.getHeight() < position.z) getControls().fallForward(position.z - block.getHeight(), false);
        else getControls().forward();
    }

    boolean isPathOnFront(boolean canFall2) {
        Position position = getControls().getPosition();
        Block block = getBlockOnFront();

        int maxHeight = position.z + 1;
        int minHeight = position.z;
        if (canFall2) minHeight -= 2;
        else minHeight -= 1;

        return (block.getHeight() <= maxHeight && block.getHeight() >= minHeight);
    }

    boolean isFrontBlockUnvisitedHighest() {
        Block block = getBlockOnFront();
        return (block.isNotVisited()) && (block.getHeight() >= field.highestHigh);
    }

    boolean isBlockInDirectionExact(Direction direction) {
        Position position = getControls().getPosition();
        Position blockPosition = position.atDirection(direction);
        Block block = field.getOnPosition(blockPosition);
        return block.isExact();
    }

    boolean isNotBlockInDirectionExact(Direction direction) {
        return !isBlockInDirectionExact(direction);
    }

    void rotateLeftSmart() {
        getControls().rotateLeft();
    }

    void rotateRightSmart() {
        getControls().rotateRight();
    }

    void lookAroundSmart(boolean lookFirstRight) {
        if (lookFirstRight) {
            if (lookRightSmart()) return;
            lookLeftSmart();
        } else {
            if (lookLeftSmart()) return;
            lookRightSmart();
        }
    }

    boolean lookRightSmart() {
        Direction direction = getControls().getDirection();
        Position position = getControls().getPosition();

        if (isNotBlockInDirectionExact(direction.right())) {
            rotateRightSmart();
            testUltrasonic();

            if (getField().getOnPosition(position.atDirection(direction.right())).getHeight() >= field.highestHigh) {
                return true;
            }

            rotateLeftSmart();
        }

        return false;
    }

    boolean lookLeftSmart() {
        Direction direction = getControls().getDirection();
        Position position = getControls().getPosition();

        if (isNotBlockInDirectionExact(direction.left())) {
            rotateLeftSmart();
            testUltrasonic();

            if (getField().getOnPosition(position.atDirection(direction.left())).getHeight() >= field.highestHigh) {
                return true;
            }

            rotateRightSmart();
        }

        return false;
    }


    void markCurrentBlockAsVisited() {
        Position position = getControls().getPosition();
        Block block = field.getOnPosition(position);

        if (block.isVisited()) return;

        block.setVisited();
        if (block.getHeight() >= field.highestHigh) field.highestFound ++;
    }


    Block getBlockOnFront() {
        Position position = getControls().getPosition();
        Direction direction = getControls().getDirection();
        Position blockPosition = position.atDirection(direction);

        return field.getOnPosition(blockPosition);
    }


    void printFieldWithPosition() {
        if (IS_DEBUG) {
            Position position = getControls().getPosition();

            System.out.println("P" + position);
            System.out.println("D: " + getControls().getDirection());
            System.out.println(field.toString(position));
            if (IS_VIRTUAL) System.out.println();
        }
    }


    // Find one of the highest blocks, that are closest to the robot
    Position findPossibleHighestBlockPosition(PathFinder pathFinder, boolean rightFirst) {
        List<Block> highestBlocks = new ArrayList<Block>();
        int highestBlockHeight = 0;

        if (rightFirst) {
            for (int y = 1; y < field.sizeY; y++) {
                for (int x = field.sizeX - 1; x >= 1; x--) {

                    Block block = field.field[y][x];
                    if (block.visited) continue;

                    if (block.getHeight() > highestBlockHeight) {
                        highestBlocks.clear();
                        highestBlockHeight = block.getHeight();
                    }
                    if (block.getHeight() == highestBlockHeight) {
                        highestBlocks.add(block);
                    }

                }
            }
        } else {
            for (int y = 1; y < field.sizeY; y++) {
                for (int x = 1; x < field.sizeX; x++) {

                    Block block = field.field[y][x];
                    if (block.visited) continue;

                    if (block.getHeight() > highestBlockHeight) {
                        highestBlocks.clear();
                        highestBlockHeight = block.getHeight();
                    }
                    if (block.getHeight() == highestBlockHeight) {
                        highestBlocks.add(block);
                    }

                }
            }
        }

        pathFinder.findPath(new Position(0, 0, 9));  // set pathFindingLength to the blocks.
        Block selectedBlock = highestBlocks.get(0);

        for (Block block : highestBlocks) {
            if (block.pathFindingLength < selectedBlock.pathFindingLength) {
                selectedBlock = block;
            }
        }

        return selectedBlock.getPosition();
    }

}
