package cz.pusici.Algorithms;

import cz.pusici.Algorithms.Utils.Block;
import cz.pusici.Algorithms.Utils.PathFinder;
import cz.pusici.HighLevelControl.Direction;
import cz.pusici.HighLevelControl.Position;

import static cz.pusici.Config.BUTTON_STEPPING_DEBUG;
import static cz.pusici.Config.IS_DEBUG;
import static cz.pusici.HighLevelControl.Controls.getControls;
import static cz.pusici.HighLevelControl.Direction.*;
import static cz.pusici.Utils.waitForKey;


/**
 * WORKS with virtual field.
 */
public class SafeToEnd extends AlgUtils implements Algorithm {

    private final boolean lookAround;
    private final boolean lookFirstRight;  // When searching for next path.
    private final boolean lookAroundHighest;
    private boolean canFall2Blocks;
    private final boolean canFall2BlocksIfAllHighestFound;
    private final boolean ratherHighestThenEnd;
    private boolean visitUnvisitedArountHighestFirst;
    private boolean rightFirst;
    private PathFinder pathFinder;

    public SafeToEnd(boolean lookAround, boolean lookAroundHighest,
                     boolean canFall2Blocks, boolean canFall2BlocksIfAllHighestFound,
                     boolean ratherHighestThenEnd, boolean visitUnvisitedArountHighestFirst,
                     boolean rightFirst) {
        super();
        this.lookAround = lookAround;
        this.lookFirstRight = true;
        this.canFall2Blocks = canFall2Blocks;
        this.canFall2BlocksIfAllHighestFound = canFall2BlocksIfAllHighestFound;
        this.ratherHighestThenEnd = ratherHighestThenEnd;
        this.visitUnvisitedArountHighestFirst = visitUnvisitedArountHighestFirst;
        this.lookAroundHighest = lookAroundHighest;
        this.rightFirst = rightFirst;
        initializePathFinder();
    }

    private void initializePathFinder() {
        int maxFall = 1;
        if (canFall2Blocks) maxFall = 2;
        pathFinder = new PathFinder(field, maxFall, rightFirst, new PathFinder.IsTarget() {
            @Override
            public boolean isTarget(Position position) {
                return position.y >= field.sizeY;
            }
        });
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void start() {
        System.out.println("ALG: SafeToEnd");
        printFieldWithPosition();


        // Continue normally
        while (true) {
            if (canFall2BlocksIfAllHighestFound && field.highestFound >= 4) {
                canFall2Blocks = true;
                pathFinder.maxFall = 2;
            }

            // Check end conditions
            if (getControls().getPosition().y == 9) return;  // End after robot is at the end

            // Check sensors
            testUltrasonicSmart();

            // Mark blocks
            markCurrentBlockAsVisited();
            printFieldWithPosition();
            if (BUTTON_STEPPING_DEBUG) waitForKey();

            // Check sides
            if (isPathOnFront(canFall2Blocks)) {
                if (ratherHighestThenEnd && isFrontBlockUnvisitedHighest() && field.highestFound < 4) {
                    forwardOrClimbOrFall();
                    continue;
                } else if (lookAround) {
                    lookAroundSmart(rightFirst);
                } else if (ratherHighestThenEnd && field.highestFound < 3 && lookAroundHighest
                        && field.getOnPosition(getControls().getPosition()).getHeight() >= field.highestHigh) {
                    // Robot is standing on the highest block.
                    lookAroundSmart(rightFirst);
                }
            }

            // Find best direction to the end
            Direction bestDirectionToTheEnd;
            Direction unvisitedDirection = findUnvisitedDirectionAroundRobot();
            if (visitUnvisitedArountHighestFirst && unvisitedDirection != null && field.highestFound < 4
                    && field.getOnPosition(getControls().getPosition()).getHeight() >= field.highestHigh) {
                // Going to unvisited, when standing on highest
                if (IS_DEBUG) System.out.println("                      TARGET: unvisited");
                bestDirectionToTheEnd = unvisitedDirection;
            } else if (field.highestFound < 4
                    && field.getOnPosition(findPossibleHighestBlockPosition(pathFinder, rightFirst)).getHeight() >= field.highestHigh
                    && pathFinder.findPath(
                            field.getOnPosition(findPossibleHighestBlockPosition(pathFinder, rightFirst)
                            ).getPosition()) != null) {
                // If there is path to the highest
                if (IS_DEBUG) System.out.println("                      TARGET: highest");
                bestDirectionToTheEnd = pathFinder.findPath(findPossibleHighestBlockPosition(pathFinder, rightFirst)).direction;
            } else {
                if (IS_DEBUG) System.out.println("                      TARGET: end");
                bestDirectionToTheEnd = pathFinder.findPath().direction;
            }
            if (IS_DEBUG) System.out.println("Best direction to the end: " + bestDirectionToTheEnd);

            // Move
            if (getControls().getPosition().y > field.sizeY - 1) bestDirectionToTheEnd = UP;
            switch (getControls().getDirection()) {
                case UP:
                    if (bestDirectionToTheEnd == UP) forwardOrClimbOrFall();
                    else if (bestDirectionToTheEnd == LEFT) rotateLeftSmart();
                    else if (bestDirectionToTheEnd == RIGHT) rotateRightSmart();
                    else if (bestDirectionToTheEnd == DOWN) rotateRightSmart();
                    break;
                case LEFT:
                    if (bestDirectionToTheEnd == UP) rotateRightSmart();
                    else if (bestDirectionToTheEnd == LEFT) forwardOrClimbOrFall();
                    else if (bestDirectionToTheEnd == RIGHT) rotateLeftSmart();
                    else if (bestDirectionToTheEnd == DOWN) rotateLeftSmart();
                    break;
                case RIGHT:
                    if (bestDirectionToTheEnd == UP) rotateLeftSmart();
                    else if (bestDirectionToTheEnd == LEFT) rotateRightSmart();
                    else if (bestDirectionToTheEnd == RIGHT) forwardOrClimbOrFall();
                    else if (bestDirectionToTheEnd == DOWN) rotateRightSmart();
                    break;
                case DOWN:
                    if (bestDirectionToTheEnd == UP) rotateLeftSmart();
                    else if (bestDirectionToTheEnd == LEFT) rotateRightSmart();
                    else if (bestDirectionToTheEnd == RIGHT) rotateLeftSmart();
                    else if (bestDirectionToTheEnd == DOWN) forwardOrClimbOrFall();
                    break;
                default:
                    System.out.println("ERROR, no direction is best!");
                    getControls().climbForward();
            }
        }
    }

    // Find direction, in which is there a unvisited block
    public Direction findUnvisitedDirectionAroundRobot() {
        Direction[] directions = {LEFT, RIGHT, DOWN, UP};
        Position position = getControls().getPosition();

        for (Direction direction : directions) {
            Block block = field.getOnPosition(position.atDirection(direction));
            if (block.isNotVisited()
                    && block.getHeight() <= position.z + 1
                    && block.getHeight() >= position.z - 1) {
                return direction;
            }
        }

        return null;
    }

}
