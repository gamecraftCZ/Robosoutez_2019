package cz.pusici.tests;

import lejos.nxt.Button;
import lejos.nxt.Sound;

import static cz.pusici.HighLevelControl.Controls.getControls;
import static cz.pusici.LowLevelControl.Motors.getMotors;
import static cz.pusici.LowLevelControl.Sensors.getSensors;

public class MovementTest {

    public static void main(String[] args) {
        Sound.setVolume(100);

        getSensors().gyro.setCurrentRotation(0);
        getControls();
        Sound.beep();

        while (true) {
            if (Button.LEFT.isDown()) getControls().forward();
            else if (Button.RIGHT.isDown()) getControls().backward();
            else if (Button.ENTER.isDown()) getMotors().stop();
            else if (Button.ESCAPE.isDown()) break;

//            System.out.println("right: " + getMotors().rightMotor.getRotations());
//            System.out.println(" left: " + getMotors().leftMotor.getRotations());
//            System.out.println(" gyro: " + getSensors().gyro.getCurrentRotationRounded(2));
        }

        getMotors().stop();
        Sound.beep();
    }

}
