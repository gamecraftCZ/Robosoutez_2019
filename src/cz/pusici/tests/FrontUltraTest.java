package cz.pusici.tests;

import lejos.nxt.Button;
import lejos.nxt.Sound;

import static cz.pusici.HighLevelControl.Controls.getControls;
import static cz.pusici.LowLevelControl.Sensors.getSensors;
import static cz.pusici.Utils.sleep;

public class FrontUltraTest {

    public static void main(String[] args) {
        Sound.setVolume(100);
        Sound.beep();

        getControls();
        Sound.twoBeeps();

        while (true) {
            if (Button.waitForAnyPress() == Button.ID_ESCAPE) break;

            System.out.println("B: " + getControls().testForward());
            System.out.println("D: " + getSensors().ultrasonic.getDistance());

            sleep(200);
        }

        Sound.beep();
        System.exit(0);
    }

}
