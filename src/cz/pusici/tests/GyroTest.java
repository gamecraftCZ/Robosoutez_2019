package cz.pusici.tests;

import lejos.nxt.Button;
import lejos.nxt.Sound;

import static cz.pusici.LowLevelControl.Sensors.getSensors;
import static cz.pusici.Utils.sleep;

public class GyroTest {

    public static void main(String[] args) {
        Sound.setVolume(100);
        Sound.beep();

        getSensors().gyro.setCurrentRotation(0);
        while (true) {
            if (Button.ENTER.isDown()) getSensors().gyro.setCurrentRotation(0);
            System.out.println("rot: " + getSensors().gyro.getCurrentRotationRounded(2));
            System.out.println("vel: " + getSensors().gyro.getAngularVelocity());
            sleep(500);
        }

//        Sound.beep();
    }

}
