package cz.pusici.tests;

import lejos.nxt.Button;
import lejos.nxt.Sound;

import static cz.pusici.HighLevelControl.Controls.getControls;

public class ClimbTest {

    public static void main(String[] args) {
        Sound.setVolume(100);
        getControls();
        Sound.beep();

        System.out.println("Press enter to climb");
        System.out.println();
        System.out.println("Press escape to exit");
        while (true) {
            if (Button.waitForAnyPress() == Button.ID_ESCAPE) break;
            getControls().climbForward();
        }
//        getControls().climbForward();
//        getMotors().backMotorDown();

        Sound.beep();
        System.exit(0);
    }

}
