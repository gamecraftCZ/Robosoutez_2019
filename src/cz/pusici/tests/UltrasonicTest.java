package cz.pusici.tests;

import lejos.nxt.Button;
import lejos.nxt.Sound;

import static cz.pusici.LowLevelControl.Sensors.getSensors;

public class UltrasonicTest {

    public static void main(String[] args) {
        Sound.setVolume(100);
        Sound.beep();

        while (true) {
            System.out.println("dist: " + getSensors().ultrasonic.getDistance());
            if (Button.ESCAPE.isDown()) break;
        }

        Sound.beep();
        System.exit(0);
    }

}
