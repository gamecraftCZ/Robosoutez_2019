package cz.pusici.tests;

import lejos.nxt.Button;
import lejos.nxt.Sound;

import static cz.pusici.HighLevelControl.Controls.getControls;
import static cz.pusici.LowLevelControl.Motors.getMotors;
import static cz.pusici.LowLevelControl.Sensors.getSensors;

public class RotateTest {

    public static void main(String[] args) {
        Sound.setVolume(100);

        getSensors().gyro.setCurrentRotation(0);
        getControls();
        Sound.beep();

        while (true) {
            if (Button.LEFT.isDown()) getControls().rotateLeft();
            else if (Button.RIGHT.isDown()) getControls().rotateRight();
            else if (Button.ENTER.isDown()) getMotors().stop();
            else if (Button.ESCAPE.isDown()) break;
        }

        getMotors().stop();
        Sound.beep();
        System.exit(0);
    }

}
