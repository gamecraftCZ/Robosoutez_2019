package cz.pusici.tests;

import lejos.nxt.Button;
import lejos.nxt.Sound;

import static cz.pusici.LowLevelControl.Motors.getMotors;

public class BackMotorTest {

    public static void main(String[] args) {
        Sound.setVolume(100);
        Sound.beep();

        while (true) {
            if (Button.LEFT.isDown()) getMotors().backMotorDownInfinite();
            else if (Button.RIGHT.isDown()) getMotors().backMotorUpInfinite();
            else if (Button.ESCAPE.isDown()) break;
            else getMotors().backMotorForceStop();

            System.out.println("rot: " + getMotors().backMotor.getTachoCount());
        }

        Sound.beep();
    }

}
