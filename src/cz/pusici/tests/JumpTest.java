package cz.pusici.tests;

import lejos.nxt.Button;
import lejos.nxt.Sound;

import static cz.pusici.HighLevelControl.Controls.getControls;
import static cz.pusici.LowLevelControl.Motors.getMotors;
import static cz.pusici.Utils.sleep;

public class JumpTest {

    public static void main(String[] args) {
        Sound.setVolume(100);

        getControls();
        Sound.beep();

        while (true) {
            int buttonPressed = Button.waitForAnyPress();
            if (buttonPressed == Button.ID_ESCAPE) break;
            getMotors().forward(400);
            sleep(1000);
            getMotors().forward(1200);
            sleep(5000);
            getMotors().forward(200);
            sleep(1000);
            getMotors().stop();
        }

        getMotors().stop();
        Sound.beep();
    }

}
