package cz.pusici.tests;

import lejos.nxt.Button;
import lejos.nxt.Sound;

import static cz.pusici.LowLevelControl.Motors.getMotors;

public class RotateManualTest {

    public static void main(String[] args) {
        Sound.setVolume(100);
        getMotors();
        Sound.beep();

        System.out.println("Press enter to Rotate LEFT");
        System.out.println();
        System.out.println("Press escape to exit");
        Button.waitForAnyPress();
        while (true) {
            int button = Button.waitForAnyPress();
            if (button == Button.ID_ESCAPE) break;
            if (button == Button.ID_LEFT) getMotors().rotateLeft(430, 200);
            if (button == Button.ID_RIGHT) getMotors().rotateRight(430, 200);
        }

        //Sound.beep();
    }

}
