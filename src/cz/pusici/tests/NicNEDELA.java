package cz.pusici.tests;

import lejos.nxt.Button;
import lejos.nxt.Sound;

import static cz.pusici.HighLevelControl.Controls.getControls;

public class NicNEDELA {

    public static void main(String[] args) {
        getControls();
        Sound.setVolume(255);
        Sound.beep();

        while (Button.waitForAnyPress() != Button.ID_ESCAPE) {
            getControls().littleForward();
        }

        Sound.beep();
        System.exit(0);
    }

}
