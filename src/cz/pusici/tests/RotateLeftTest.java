package cz.pusici.tests;

import lejos.nxt.Button;
import lejos.nxt.Sound;

import static cz.pusici.HighLevelControl.Controls.getControls;

public class RotateLeftTest {

    public static void main(String[] args) {
        Sound.setVolume(100);
        getControls();
        Sound.beep();

        System.out.println("Press enter to Rotate LEFT");
        System.out.println();
        System.out.println("Press escape to exit");
        Button.waitForAnyPress();
        while (true) {
            if (Button.waitForAnyPress() == Button.ID_ESCAPE) break;
            getControls().rotateLeft();
            getControls().forward();
        }

        Sound.beep();
    }

}
