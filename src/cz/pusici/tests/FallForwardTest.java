package cz.pusici.tests;

import lejos.nxt.Button;
import lejos.nxt.Sound;

import static cz.pusici.HighLevelControl.Controls.getControls;

public class FallForwardTest {

    public static void main(String[] args) {
        Sound.setVolume(100);
        getControls();
        Sound.beep();

        System.out.println("Press enter to Fall FORWARD");
        System.out.println();
        System.out.println("Press escape to exit");
        while (true) {
            int buttonPresses = Button.waitForAnyPress();
            if (buttonPresses == Button.ID_ESCAPE) break;
            if (buttonPresses == Button.ID_LEFT) getControls().fallForward(1, false);
            if (buttonPresses == Button.ID_RIGHT) getControls().fallForward(2, false);
        }

        Sound.beep();
        System.exit(0);
    }

}
