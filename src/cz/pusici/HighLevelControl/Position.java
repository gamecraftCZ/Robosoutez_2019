package cz.pusici.HighLevelControl;

import static cz.pusici.Utils.randomInt;

public class Position {

    public byte x = 0;
    public byte y = 0;
    public byte z = 0;

    public Position(byte x, byte y, byte z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Position(int x, int y, int z) {
        this.x = (byte)x;
        this.y = (byte)y;
        this.z = (byte)z;
    }

    public Position() {}

    @Override
    public Position clone() {
        return new Position(this.x, this.y, this.z);
    }

    public boolean equals(Position position) {
        return (position.x == x && position.y == y && position.z == z);
    }

    public boolean notEquals(Position position) {
        return !equals(position);
    }

    public Position atDirection(Direction direction) {
        Position newPosition = this.clone();
        switch (direction) {
            case UP:
                newPosition.y += 1;
                break;
            case DOWN:
                newPosition.y -= 1;
                break;
            case LEFT:
                newPosition.x -= 1;
                break;
            case RIGHT:
                newPosition.x += 1;
                break;

            case UP_LEFT:
                newPosition.y += 1;
                newPosition.x -= 1;
                break;
            case UP_RIGHT:
                newPosition.y += 1;
                newPosition.x += 1;
                break;
            case DOWN_LEFT:
                newPosition.y -= 1;
                newPosition.x -= 1;
                break;
            case DOWN_RIGHT:
                newPosition.y -= 1;
                newPosition.x += 1;
                break;
        }
        return newPosition;
    }

    public static Position randomPosition(int maxX, int maxY) {
        int posX = randomInt(1, maxX + 1);
        int posY = randomInt(1, maxY + 1);
        return new Position(posX, posY, 0);
    }

    @Override
    public String toString() {
        return "(x:" + x + ",y:" + y + ",z:" + z + ")";
    }

}
