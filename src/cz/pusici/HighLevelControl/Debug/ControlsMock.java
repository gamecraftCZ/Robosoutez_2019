package cz.pusici.HighLevelControl.Debug;

import cz.pusici.HighLevelControl.Controls;
import cz.pusici.HighLevelControl.Direction;
import cz.pusici.HighLevelControl.Position;

import static cz.pusici.Utils.waitForKey;

public class ControlsMock extends Controls {

    private Position position = new Position(1, 1, 0);
    private Direction direction = Direction.UP;
    // This field mock is visually flipped by y axis. So the top is in reality at the bottom.
    public int[][] fieldMock = VirtualFields.FINALE_KOLO3;
    public VirtualField field = new VirtualField(fieldMock);

    // This is here just for TEST
    public static void main(String[] args) {
        ControlsMock controls = new ControlsMock(new Position(1, 1, 0));
        controls.forward();
        System.out.println(controls.field.toString(controls.position));
        controls.forward();
        System.out.println(controls.field.toString(controls.position));
        controls.rotateRight();
        controls.forward();
        System.out.println(controls.field.toString(controls.position));
        controls.rotateLeft();
        controls.climbForward();
        System.out.println(controls.field.toString(controls.position));
        controls.climbForward();
        System.out.println(controls.field.toString(controls.position));
        controls.rotateRight();
        controls.rotateRight();
        controls.backward();
        System.out.println(controls.field.toString(controls.position));
        controls.rotateLeft();
        controls.forward();
        controls.forward();
        controls.forward();
        controls.rotateRight();
        controls.forward();
        System.out.println(controls.field.toString(controls.position));
        controls.climbForward();
        System.out.println(controls.field.toString(controls.position));
        controls.forward();
        controls.forward();
        System.out.println(controls.field.toString(controls.position));
    }

    public ControlsMock(Position startPosition) {
        position = startPosition;
        System.out.println("Mock field:");
        System.out.println(this.field.toString(position));
        System.out.println();
    }


    @Override
    public void forward() {
        Position directionPosition = position.atDirection(direction);

        if (field.getOnPosition(directionPosition) == position.z) {
            this.position = directionPosition;
        } else {
            System.out.println("!!! ERROR !!! - There is a obstacle with height " +
                    field.getOnPosition(directionPosition) +
                    ", Tried position: " + directionPosition);
        }

        System.out.println("FORWARD - Direction: " + direction + ", Position: " + this.position);
        waitForKey();
    }
    @Override
    public void backward() {
        Position directionPosition = position.atDirection(direction.reverse());

        if (field.getOnPosition(directionPosition) == position.z) {
            this.position = directionPosition;
        } else {
            System.out.println("!!! ERROR !!! - There is a obstacle with height " +
                    field.getOnPosition(directionPosition) +
                    ", Tried position: " + directionPosition);
        }

        System.out.println("BACKWARD - Direction: " + direction + ", Position: " + this.position);
        waitForKey();
    }


    @Override
    public void rotateLeft() {
        direction = direction.left();
        System.out.println("ROTATE_LEFT - Direction: " + direction + ", Position: " + this.position);
        waitForKey();
    }
    @Override
    public void rotateRight() {
        direction = direction.right();
        System.out.println("ROTATE_RIGHT - Direction: " + direction + ", Position: " + this.position);
        waitForKey();
    }

    @Override
    public void rotateHalfRight() {
        direction = direction.halfRight();
        System.out.println("ROTATE_HALF_RIGHT - Direction: " + direction + ", Position: " + this.position);
        waitForKey();
    }
    @Override
    public void rotateHalfLeft() {
        direction = direction.halfLeft();
        System.out.println("ROTATE_HALF_LEFT - Direction: " + direction + ", Position: " + this.position);
        waitForKey();
    }


    @Override
    public void climbForward() {
        Position directionPosition = position.atDirection(direction);
        directionPosition.z += 1;

        if (field.getOnPosition(directionPosition) == position.z + 1) {
            this.position = directionPosition;
        } else {
            System.out.println("!!! ERROR !!! - There is a obstacle with height " +
                    field.getOnPosition(directionPosition) +
                    ", Tried position: " + directionPosition);
        }
        System.out.println("CLIMB_FORWARD - Direction: " + direction + ", Position: " + this.position);

        waitForKey();
    }
    @Override
    public void fallForward(int blocksFall, boolean fastFall) {
        Position directionPosition = position.atDirection(direction);
        directionPosition.z -= blocksFall;

        if (field.getOnPosition(directionPosition) < position.z + 2) {
            this.position = directionPosition;
        } else {
            System.out.println("!!! ERROR !!! - There is a obstacle with height " +
                    field.getOnPosition(directionPosition) +
                    ", Tried position: " + directionPosition);
        }
        System.out.println("FALL_FORWARD - Direction: " + direction + ", Position: " + this.position);

        waitForKey();
    }


    @Override
    public int testForward() {
        Position directionPosition = position.atDirection(direction);
        int checkingWidth = field.getOnPosition(directionPosition);

        int relativeHeight = checkingWidth - position.z;

        System.out.println("TEST_ULTRASONIC - Direction: " + direction + ", Position: " + this.position
                            + ", Relative height: " + relativeHeight);
        waitForKey();
        return relativeHeight;
    }


    @Override
    public void littleForward() { }
    @Override
    public void littleBackward() { }


    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public Direction getDirection() {
        return direction;
    }

}
