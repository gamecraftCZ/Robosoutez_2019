package cz.pusici.HighLevelControl.Debug;


import cz.pusici.HighLevelControl.Position;

import static java.lang.Math.random;

/**
 * In original rules there is 9 by 6 playing field.
 * We use it one larger with the ones on sides are really high so robot will never try to climb them.
 */
public class VirtualField {
    public final int[][] field;


    /**
     * Creates random field. Can be uncompletable.
     * @param sizeX Size of the playing field on X axis.
     * @param sizeY Size of the playing field on Y axis.
     */
    public VirtualField(int sizeX, int sizeY) {
        int[][] newField = new int[sizeY+2][sizeX+2];

        // Fill all with 9 high walls.
        for (int yAxis = 0; yAxis < sizeY+2; yAxis++) {
            for (int xAxis = 0; xAxis < sizeX+2; xAxis++) {
                newField[yAxis][xAxis] = 9;
            }
        }

        // Fill start and finish with 0 high walls.
        for (int xAxis = 1; xAxis < sizeX+1; xAxis++) {
            newField[1][xAxis] = 0;
            newField[sizeY][xAxis] = 0;
        }

        // Fill the rest of play area with 0-4 high walls.
        for (int yAxis = 2; yAxis < sizeY; yAxis++) {
            for (int xAxis = 1; xAxis < sizeX+1; xAxis++) {
                newField[yAxis][xAxis] = (int)(5 * random());
            }
        }

        field = newField;
    }
    public VirtualField(int[][] field) {
        int sizeY = field.length;
        int sizeX = field[0].length;
        int[][] newField = new int[sizeY+2][sizeX+2];

        for (int yAxis = 0; yAxis < sizeY+2; yAxis++) {
            for (int xAxis = 0; xAxis < sizeX+2; xAxis++) {
                newField[yAxis][xAxis] = 9;
            }
        }

        for (int yAxis = 1; yAxis < sizeY+1; yAxis++) {
            for (int xAxis = 1; xAxis < sizeX+1; xAxis++) {
                newField[yAxis][xAxis] = field[yAxis-1][xAxis-1];
            }
        }

        this.field = newField;
    }

    public int getOnPosition(Position position) {
        return field[position.y][position.x];
    }

    public String toString() {
        StringBuilder fieldString = new StringBuilder();
        for (int yAxis = field.length - 1; yAxis >= 0; yAxis--) {
            fieldString.append(yAxis).append(".  ");
            for (int xAxis = 0; xAxis < field[0].length; xAxis++) {
                fieldString.append(field[yAxis][xAxis]).append("  ");
            }
            fieldString.append("\n");
        }
        fieldString.append("y x ");
        for (int xAxis = 0; xAxis < field[0].length; xAxis++) {
            fieldString.append(xAxis).append(". ");
        }
        return fieldString.toString();
    }

    public String toString(Position highlight) {
        StringBuilder fieldString = new StringBuilder();
        for (int yAxis = field.length - 1; yAxis >= 0; yAxis--) {
            fieldString.append(yAxis).append(".  ");
            for (int xAxis = 0; xAxis < field[0].length; xAxis++) {
                fieldString.append(field[yAxis][xAxis]);
                if (highlight.x == xAxis && highlight.y == yAxis) fieldString.append("x ");
                else fieldString.append("  ");
            }
            fieldString.append("\n");
        }
        fieldString.append("y x ");
        for (int xAxis = 0; xAxis < field[0].length; xAxis++) {
            fieldString.append(xAxis).append(". ");
        }
        return fieldString.toString();
    }

}
