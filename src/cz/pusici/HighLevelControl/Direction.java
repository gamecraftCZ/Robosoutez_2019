package cz.pusici.HighLevelControl;

public enum Direction {
    UP(0),
    UP_RIGHT(1),
    RIGHT(2),
    DOWN_RIGHT(3),
    DOWN(4),
    DOWN_LEFT(5),
    LEFT(6),
    UP_LEFT(7);


    protected int numberRepresentation;

    Direction(int numberRepresentation) {
        this.numberRepresentation = numberRepresentation;
    }

    public Direction reverse() {
        return getFromNumber(numberRepresentation + 4);
    }

    public Direction right() {
        return getFromNumber(numberRepresentation + 2);
    }

    public Direction left() {
        return getFromNumber(numberRepresentation - 2);
    }

    public Direction halfRight() {
        return getFromNumber(numberRepresentation + 1);
    }

    public Direction halfLeft() {
        return getFromNumber(numberRepresentation - 1);
    }


    private static Direction getFromNumber(int number) {
        number = number % 8;
        if (number < 0) number = number + 8;

        for (Direction direction : Direction.values()) {
            if (direction.numberRepresentation == number) return direction;
        }
        return null;
    }

}
