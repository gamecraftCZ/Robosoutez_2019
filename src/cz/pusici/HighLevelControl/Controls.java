package cz.pusici.HighLevelControl;

import cz.pusici.HighLevelControl.Debug.ControlsMock;

import static cz.pusici.Config.*;
import static cz.pusici.HighLevelControl.Direction.UP;
import static cz.pusici.LowLevelControl.Motors.getMotors;
import static cz.pusici.LowLevelControl.Sensors.getSensors;
import static cz.pusici.Utils.sleep;


public class Controls {
    private static Controls instance;

    private Position position = new Position(1, 1, 1);
    private Direction direction = UP;

    private int motorsTargetRotation = 0;
    private float littleForwardTimes = 0;


    public Controls() {
        if (!IS_VIRTUAL) {
            getMotors();
            getSensors();
        }
    }

    public static Controls getControls() {
        if (instance == null) instance = new Controls();
        return instance;
    }
    public static Controls createMockControls(Position startPosition) {
        instance = new ControlsMock(startPosition);
        return instance;
    }

    public Direction getDirection() {
        return direction;
    }

    /**
     * Moves the robot one block Forward. No climbing, no falling.
     */
    public void forward() {
        if (IS_DEBUG) System.out.println("FORWARD");
        position = position.atDirection(direction);

        int rotationNeeded = (int) (BLOCK_MOTOR_ROTATIONS - littleForwardTimes * LITTLE_FORWARD);
        littleForwardTimes = 0;
        getMotors().forwardRotation(rotationNeeded, true);
        motorsTargetRotation += rotationNeeded;

        System.out.println("m: " + getMotors().getMovementRotations());
        System.out.println("t: " + motorsTargetRotation);

        while (getMotors().isMoving() && getMotors().getMovementRotations() < motorsTargetRotation - 70);
        getMotors().setSpeed(400);
        getMotors().waitComplete();
    }
    /**
     * Moves the robot one block Backward. No climbing, no falling.
     */
    public void backward() {
        if (IS_DEBUG) System.out.println("BACKWARD");
        position = position.atDirection(direction.reverse());

        int rotationNeeded = (int) (BLOCK_MOTOR_ROTATIONS + littleForwardTimes * LITTLE_FORWARD);
        littleForwardTimes = 0;
        getMotors().backwardRotation(rotationNeeded, true);
        motorsTargetRotation -= rotationNeeded;

        while (getMotors().isMoving() && getMotors().getMovementRotations() > motorsTargetRotation + 70);
        getMotors().setSpeed(400);
        getMotors().waitComplete();
        sleep(50);
    }


    private void rotateLeft(int degrees) {
        if (IS_DEBUG) System.out.println("ROTATE_LEFT_" + degrees);

        // Wait to finish previous movement
        while (getMotors().isMoving() && getMotors().getMovementRotations() != motorsTargetRotation);
        littleMoveBack();

        // Rotate
        getMotors().rotateLeft(ROTATION_90_DEGREES * degrees / 90, 300);

        getMotors().backwardRotation(50, 200);
    }
    private void rotateRight(int degrees) {
        if (IS_DEBUG) System.out.println("ROTATE_RIGHT_" + degrees);

        // Wait to finish previous movement
        while (getMotors().isMoving() && getMotors().getMovementRotations() != motorsTargetRotation);
        littleMoveBack();

        // Rotate
        getMotors().rotateRight(ROTATION_90_DEGREES * degrees / 90, 300);
        getMotors().backwardRotation(50, 200);
    }

    /**
     * Rotates the robot by 90 degrees Right.
     */
    public void rotateRight() {
        rotateRight(90);
        direction = direction.right();
    }

    /**
     * Rotates the robot by 90 degrees Left.
     */
    public void rotateLeft() {
        rotateLeft(90);
        direction = direction.left();
    }

    /**
     * Rotates the robot by 45 degrees Right.
     */
    public void rotateHalfRight() {
        rotateRight(45);
        direction = direction.halfRight();
    }

    /**
     * Rotates the robot by 45 degrees Left.
     */
    public void rotateHalfLeft() {
        rotateLeft(45);
        direction = direction.halfLeft();
    }


    /**
     * Climbs one block Forward.
     */
    public void climbForward() {
        if (IS_DEBUG) System.out.println("CLIMB_FORWARD");
        position = position.atDirection(direction);
        getSensors().gyro.setCurrentRotation(0);

        // Ride to the block
        getMotors().forward(CLIMB_SPEED);
//        getMotors().backMotorRotateTo(0.2f);
        getSensors().gyro.waitForAbsoluteRotation(5, 2000);

        // Prepare for climb
        getMotors().backMotorRotateTo(0.3f);
        getSensors().gyro.waitForAbsoluteRotation(50, 500);
        sleep(300);

        // Climb
        getMotors().backMotorDown();
        getMotors().forwardRotation(240, CLIMB_SPEED / 2, true);
        getMotors().backMotorUp();
        getMotors().forwardRotation(30, CLIMB_SPEED / 2);

        littleForwardTimes = 0;
        position.z += 1;
    }

    /**
     * Falls one or two blocks Forward.
     */
    public void fallForward(int blocksFall, boolean fastFall) {
        if (IS_DEBUG) System.out.println("FALL_FORWARD");
        littleForwardTimes = 0;
        position = position.atDirection(direction);
        getSensors().gyro.setCurrentRotation(0);

        // Fall
        if (fastFall) {
            fastFall();
        } else {
            slowFall(blocksFall);
        }

        goToBlockCenterAfterFall();

        position.z -= blocksFall;
    }
    private void fastFall() {
        // Fall
        getMotors().forward(FALL_SPEED * 3);
        getSensors().gyro.waitForAbsoluteRotation(30, 1500);
        sleep(700);
        getMotors().stop();
    }
    private void slowFall(int blocksFall) {
        // Fall
        getMotors().backMotorRotateTo(0.2f);
        getMotors().forward(FALL_SPEED * 1 / 2);
        getSensors().gyro.waitForAbsoluteRotation(20, 2500);

        // Stand up
        getMotors().stop();
        sleep(400);
        if (blocksFall <= 1) {
            getMotors().backMotorRotateTo(0.7f);
        } else {
            // fall 2 blocks
            getMotors().backMotorRotateTo(0.4f);
        }

        sleep(200);
        if (blocksFall <= 1) {
            getMotors().backwardRotation(100, 900, true);
            sleep(200);
            getMotors().forwardRotation(400, 900);
        } else {
            // fall 2 blocks
            getMotors().backwardRotation(400, FALL_SPEED);
            getMotors().stop();
            getMotors().forwardRotation(200, FALL_SPEED);
            sleep(400);

            getMotors().backMotorRotateTo(0.7f);
            getMotors().backwardRotation(100, 900, true);
            sleep(200);
            getMotors().forwardRotation(400, 900);
        }
        getMotors().backMotorUp();
    }
    private void goToBlockCenterAfterFall() {
        // Go to the block center
        sleep(100);
        getSensors().gyro.setCurrentRotation(0);

        getMotors().backward(300);
        getSensors().gyro.waitForAbsoluteRotation(15, 2000);
        getMotors().forwardRotation(140, 200);
    }


    /**
     * Checks for relative block height of block in front of the robot.
     * @return Height of block in front of the robot, relative to robot height.
     */
    public int testForward() {
        if (IS_DEBUG) System.out.println("TEST_FORWARD");
        littleMoveBack();
        sleep(200);

        int ultrasonicBlocks = testUltrasonic();
        if (ultrasonicBlocks != 0) {
            if (ultrasonicBlocks < 0) {
                littleForwardTimes = 1.4f;
            } else if (ultrasonicBlocks == 1) {
                littleForwardTimes = 1.2f;
                littleMoveBack();
            } else if (ultrasonicBlocks == 2) {
                littleForwardTimes = 0.8f;
                littleMoveBack();
            }
            motorsTargetRotation = getMotors().getMovementRotations();
        }

        return ultrasonicBlocks;
    }

    private int testUltrasonic() {
        int blocks = getUltrasonicRelativeBlocks();
        int sensorsWait = waitForMotorsCompleteOrFrontSensor();
        if (sensorsWait > 0) return sensorsWait;

        while (littleForwardTimes < 3) {
            System.out.println("blocks: " + blocks);
            if (blocks == 0) {
                littleForward(true);
                sensorsWait = waitForMotorsCompleteOrFrontSensor();
                if (sensorsWait > 0) return sensorsWait;
                sleep(50);
                blocks = getUltrasonicRelativeBlocks();
            } else {
                getSensors().gyro.setCurrentRotation(0);
                motorsTargetRotation = getMotors().getMovementRotations();
                getMotors().forwardRotation(LITTLE_FORWARD, 100, true);
                sleep(200);

                sensorsWait = waitForMotorsCompleteOrGyroOrFrontSensors();

                getMotors().stop();
                sleep(100);
                if (sensorsWait > 0) return sensorsWait;
                boolean onTheEdge = (sensorsWait == -1);

                blocks = getUltrasonicRelativeBlocks();

                if (onTheEdge) {
                    getMotors().backwardRotation(getMotors().getMovementRotations() - motorsTargetRotation, 300);
                    break;
                } else {
                    littleForwardTimes ++;
                }
            }
        }

        return blocks;
    }
    private int getUltrasonicRelativeBlocks() {
        // Heights
        //  0 = 13
        //
        // -1 = 19
        // -2 = 24
        // -3 = 34
        int distance = getSensors().ultrasonic.getDistance();
        if (IS_DEBUG) System.out.println("Ultra: " + distance);
        return getBlocksFromDistance(distance);
    }
    private int getBlocksFromDistance(int distance) {
        if (distance < 17) return 0;
        if (distance < 22) return -1;
        if (distance < 30) return -2;
        if (distance < 38) return -3;
        if (distance < 46) return -4;
        if (distance < 54) return -5;
        return -10;
    }

    public void littleForward() {
        littleForward(false);
    }
    public void littleForward(boolean immediateReturn) {
        getMotors().forwardRotation(LITTLE_FORWARD, 300, immediateReturn);
        littleForwardTimes ++;
    }
    public void littleBackward() {
        getMotors().backwardRotation(LITTLE_FORWARD, 300);
        littleForwardTimes --;
    }
    public void littleMoveBack() {
        getMotors().backwardRotation((int)(LITTLE_FORWARD * littleForwardTimes), 300);
        littleForwardTimes = 0;
    }


    private int waitForMotorsCompleteOrFrontSensor() {
        return waitForMotorsCompleteOrSensor(true, true);
    }
    private int waitForMotorsCompleteOrSensor(boolean touch, boolean light) {
        while (getMotors().isMoving()) {
            if (light) {
                if (getSensors().frontLight.getLightValue() < FRONT_LIGHT_THRESHOLD) {
                    return 2;
                }
            }
            if (touch) {
                if (getSensors().frontTouch.isPressed()) {
                    return 1;
                }
            }
        }

        return 0;
    }

    private int waitForMotorsCompleteOrGyroOrFrontSensors() {
        while (getMotors().isMoving()) {
            if (Math.abs(getSensors().gyro.getLastValue()) > 7)                     return -1;
            if (getSensors().frontLight.getLightValue() < FRONT_LIGHT_THRESHOLD)    return 2;
            if (getSensors().frontTouch.isPressed())                                return 1;
        }

        return 0;
    }


    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

}
