package cz.pusici.LowLevelControl;

import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.TachoMotorPort;


// TODO make positionOffset for all NXTRegulatedMotor methods.
public class MyMotor extends NXTRegulatedMotor {

    private int positionOffset;

    public MyMotor(TachoMotorPort port) {
        super(port);
    }

    public int getRotations() {
        return super.getTachoCount() - positionOffset;
    }

    @Override
    public void rotateTo(int limitAngle, boolean immediateReturn) {
        super.rotateTo(limitAngle - positionOffset, immediateReturn);
    }

    @Override
    public void rotateTo(int limitAngle) {
        super.rotateTo(limitAngle - positionOffset);
    }

    public void resetPosition() {
        positionOffset = super.getPosition();
    }

}
