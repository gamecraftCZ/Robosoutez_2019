package cz.pusici.LowLevelControl;

import cz.pusici.Config;

import static cz.pusici.Config.*;
import static cz.pusici.Utils.sleep;

public class Motors {
    private static Motors instance;

    private int speed = 0;

    public MyMotor rightMotor = new MyMotor(Config.RIGHT_MOTOR_PORT);
    public MyMotor leftMotor = new MyMotor(Config.LEFT_MOTOR_PORT);
    public MyMotor backMotor = new MyMotor(Config.BACK_MOTOR_PORT);

    private Motors() {
    	setAcceleration(90000000);// TODO decide how fast accelerate

    	backMotor.setAcceleration(3000); // TODO decide best backMotor acceleration
        backMotorUpInfinite();
        sleep(500);
        backMotor.stop();
        sleep(100);
//        backMotor.rotate(-30);
        backMotor.resetTachoCount();
    }

    public static Motors getMotors() {
        if (instance == null) instance = new Motors();
        return instance;
    }


    public boolean isMoving() {
        return leftMotor.isMoving() || rightMotor.isMoving();
    }
    public int getMovementRotations() {
        return (leftMotor.getRotations() + rightMotor.getRotations()) / 2;
    }

    public void waitComplete() {
        leftMotor.waitComplete();
        rightMotor.waitComplete();
    }


    public void setSpeed(int speed) {
        this.speed = speed;
        leftMotor.setSpeed(speed);
        rightMotor.setSpeed(speed);
    }
    public int getSpeed() {
        return speed;
    }

    public void setAcceleration(int acceleration) {
        leftMotor.setAcceleration(acceleration);
        rightMotor.setAcceleration(acceleration);
    }
    public int getAcceleration() {
        return (leftMotor.getAcceleration() + rightMotor.getAcceleration()) / 2;
    }


    public void forward() {
        forward(MOVEMENT_SPEED);
    }
    public void forward(int speed) {
        setSpeed(speed);
        leftMotor.forward();
        rightMotor.forward();
    }
    public void forwardRotation(int degrees) {
        forwardRotation(degrees, MOVEMENT_SPEED);
    }
    public void forwardRotation(int degrees, boolean immediateReturn) {
        forwardRotation(degrees, MOVEMENT_SPEED, immediateReturn);
    }
    public void forwardRotation(int degrees, int speed) {
        forwardRotation(degrees, speed, false);
    }
    public void forwardRotation(int degrees, int speed, boolean immediateReturn) {
        setSpeed(speed);
        leftMotor.rotate(degrees, true);
        rightMotor.rotate(degrees, immediateReturn);
    }


    public void backward() {
        backward(MOVEMENT_SPEED);
    }
    public void backward(int speed) {
        setSpeed(speed);
        leftMotor.backward();
        rightMotor.backward();
    }
    public void backwardRotation(int degrees) {
        backwardRotation(degrees, MOVEMENT_SPEED);
    }
    public void backwardRotation(int degrees, boolean immediateReturn) {
        backwardRotation(degrees, MOVEMENT_SPEED, immediateReturn);
    }
    public void backwardRotation(int degrees, int speed) {
        backwardRotation(degrees, speed, false);
    }
    public void backwardRotation(int degrees, int speed, boolean immediateReturn) {
        setSpeed(speed);
        leftMotor.rotate(-degrees, true);
        rightMotor.rotate(-degrees, immediateReturn);
    }


    public void stop() {
        leftMotor.stop(true);
        rightMotor.stop();
    }


    public void rotateRight() {
        rotateRight(ROTATION_SPEED);
    }
    public void rotateRight(int speed) {
        setSpeed(speed);
        leftMotor.forward();
        rightMotor.backward();
    }
    public void rotateRight(int degrees, int speed) {
        setSpeed(speed);
        leftMotor.rotate(degrees, true);
        rightMotor.rotate(-degrees);
    }


    public void rotateLeft() {
        rotateLeft(ROTATION_SPEED);
    }
    public void rotateLeft(int speed) {
        setSpeed(speed);
        leftMotor.backward();
        rightMotor.forward();
    }
    public void rotateLeft(int degrees, int speed) {
        setSpeed(speed);
        leftMotor.rotate(-degrees, true);
        rightMotor.rotate(degrees);
    }


    public void backMotorUp() {
        backMotorUp(BACK_MOTOR_SPEED);
    }
    public void backMotorUp(int speed) {
        backMotor.setSpeed(speed);
        backMotor.rotateTo(0);
    }
    public void backMotorUpInfinite() {
        backMotor.forward();
    }

    public void backMotorDown() {
        backMotorDown(BACK_MOTOR_SPEED);
    }
    public void backMotorDown(int speed) {
        backMotor.setSpeed(speed);
        backMotor.rotateTo(BACK_MOTOR_DOWN_ROTATION);
    }
    public void backMotorDownInfinite() {
        backMotor.backward();
    }

    public void backMotorForceStop() {
        backMotor.stop();
    }

    public void backMotorRotateTo(float anglePart) {
        backMotorRotateTo(anglePart, BACK_MOTOR_SPEED);
    }
    public void backMotorRotateTo(float anglePart, int speed) {
        backMotor.setSpeed(speed);
        backMotor.rotateTo((int)(BACK_MOTOR_DOWN_ROTATION * anglePart));
    }

}
