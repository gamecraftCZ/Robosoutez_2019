package cz.pusici.LowLevelControl;

import com.sun.istack.internal.NotNull;
import lejos.nxt.ADSensorPort;
import lejos.nxt.addon.GyroSensor;
import lejos.util.Delay;

import static cz.pusici.LowLevelControl.Sensors.getSensors;

public class MyGyro extends GyroSensor {
    private Thread rotationThread;
    private double currentRotation = 0;
    private int lastValue = 0;
    private final Object currentRotationLock = new Object();

    private void initialize() {
        rotationThread = new GyroRotationReaderThread(this);
        rotationThread.setName("gyro thread");
        rotationThread.start();
    }

    private class GyroRotationReaderThread extends Thread {
        private GyroSensor gyro;

        @Override
        public void run() {
            long time;
            long end;
            long elapsed;
            int value;
            time = System.nanoTime();
            while (true) {
                synchronized (currentRotationLock) {
                    value = gyro.readValue();
                    lastValue = value;

                    end = System.nanoTime();
                    elapsed = end - time;
                    time = end;

                    if (Math.abs(value) > 1) {
                        currentRotation += (double) (value * elapsed) / 1000000000;
                    }

                }
            }
        }

        private GyroRotationReaderThread(GyroSensor gyro) {
            this.gyro = gyro;
        }
    }

    public double getCurrentRotation() {
        return currentRotation;
    }

    public double getCurrentRotationRounded(int numberOfPlaces) {
        if (numberOfPlaces < 0) return currentRotation;
        double multiplicatior = Math.pow(10, numberOfPlaces);
        return Math.round(getSensors().gyro.getCurrentRotation() * multiplicatior) / multiplicatior;
    }

    public void setCurrentRotation(double currentRotation) {
        synchronized (currentRotationLock) {
            this.currentRotation = currentRotation;
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    public void waitForAbsoluteRotation(float targetDegrees) {
        while (Math.abs(getCurrentRotation()) < targetDegrees);
    }

    public void waitForAbsoluteRotation(float targetDegrees, int timeout) {
        long currentTime = System.currentTimeMillis();
        while (Math.abs(getCurrentRotation()) < targetDegrees) {
            if (currentTime + timeout <= System.currentTimeMillis()) {
                return;
            }
        }
    }

    /**
     * @param timeout Timeout in milliseconds.
     * @return True if returned due to gyro started moving. False if timeout.
     */
    public boolean waitUntilGyroStartsMoving(int timeout) {
        long currentTime = System.currentTimeMillis();
        while (Math.abs(lastValue) < 9) {
            System.out.println(lastValue);
            if (currentTime + timeout <= System.currentTimeMillis()) {
                return false;
            }
        }
        System.out.println(lastValue);
        return true;
    }

    public int getLastValue() {
        return lastValue;
    }

    public int calibrate() {
        int sum = 0;
        for(int i = 0; i < 1000; ++i) {
            sum += this.readValue();
            Delay.msDelay(5L);
        }

        int offset = sum / 1000;
        this.setOffset(offset);
        return offset;
    }


    public MyGyro(@NotNull ADSensorPort port) {
        super(port);
        initialize();
    }

    public MyGyro(@NotNull ADSensorPort port, int offset) {
        super(port, offset);
        initialize();
    }

}
