package cz.pusici.LowLevelControl;

import cz.pusici.Config;
import lejos.nxt.LightSensor;
import lejos.nxt.TouchSensor;
import lejos.nxt.UltrasonicSensor;

public class Sensors {
    private static Sensors instance;

    public TouchSensor frontTouch = new TouchSensor(Config.FRONT_TOUCH_SENSOR_PORT);
    public LightSensor frontLight = new LightSensor(Config.FRONT_LIGHT_SENSOR_PORT);
    public MyGyro gyro = new MyGyro(Config.GYRO_SENSOR_PORT);
    public UltrasonicSensor ultrasonic = new UltrasonicSensor(Config.ULTRASONIC_SENSOR_PORT);


    private Sensors() {
        gyro.calibrate();
        frontLight.setFloodlight(false);
    }

    public static Sensors getSensors() {
        if (instance == null) instance = new Sensors();
        return instance;
    }

}
