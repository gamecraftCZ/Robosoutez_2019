package cz.pusici;

import cz.pusici.HighLevelControl.Position;
import lejos.nxt.Button;

import static cz.pusici.Config.IS_VIRTUAL;
import static java.lang.Math.random;

public class Utils {

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {}
    }

    public static void waitForKey() {
        if (IS_VIRTUAL) {
//            try {
//                System.in.read();
//            } catch (IOException e) {
//                return;
//            }
        } else {
            Button.waitForAnyPress();
        }
    }

    public static int randomInt(int min, int max) {
        return ((int) (random() * (max - min))) + min;
    }


    public static Position getStartPositionFromUser() {
        Position position = new Position(1, 1, 0);
        while (true) {
            System.out.println("P" + position);
            int button = Button.waitForAnyPress();
            if (button == Button.ID_ENTER) break;
            if (button == Button.ID_LEFT) position.x --;
            if (button == Button.ID_RIGHT) position.x ++;
        }
        return position;
    }

    public static int getHighestBlockHeight() {
        int highestBlock = 2;
        while (true) {
            System.out.println("Highest: " + highestBlock);
            int button = Button.waitForAnyPress();
            if (button == Button.ID_ENTER) break;
            if (button == Button.ID_LEFT) highestBlock --;
            if (button == Button.ID_RIGHT) highestBlock ++;
        }
        return highestBlock;
    }

    public static boolean getColorSensorPosition() {
        boolean colorOnRight = true;
        while (true) {
            System.out.println("C rig: " + colorOnRight);
            int button = Button.waitForAnyPress();
            if (button == Button.ID_ENTER) break;
            if (button == Button.ID_LEFT) colorOnRight = !colorOnRight;
            if (button == Button.ID_RIGHT) colorOnRight = !colorOnRight;
        }
        return colorOnRight;
    }

}
