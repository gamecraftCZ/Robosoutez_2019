package cz.pusici;


import cz.pusici.Algorithms.Algorithm;
import cz.pusici.Algorithms.FindHighest;
import cz.pusici.Algorithms.SafeToEnd;
import cz.pusici.Algorithms.Utils.Field;
import cz.pusici.HighLevelControl.Position;
import lejos.nxt.Sound;

import static cz.pusici.Algorithms.Utils.Field.createField;
import static cz.pusici.Config.IS_VIRTUAL;
import static cz.pusici.HighLevelControl.Controls.createMockControls;
import static cz.pusici.HighLevelControl.Controls.getControls;
import static cz.pusici.Utils.waitForKey;

public class Main {

    public static void main(String[] args) {
        if (IS_VIRTUAL) {
            // Initialize mock controls
            createMockControls(new Position(1, 1, 0));
        } else {
            // Initialize controls
            System.out.println("Press to calib.");
            waitForKey();
            getControls();

            Sound.setVolume(100);
            Sound.beep();

            Position position = Utils.getStartPositionFromUser();
            getControls().setPosition(position);
            System.out.println("Start position: ");
            System.out.println(position);

        }


        ////////////////////
        int highest = 3; // I

        boolean Right_first = false; // I
        boolean Look_around_highest = true; // I
        boolean rather_highest_then_end = true;
        boolean visit_unvisited_around_highest_first = false;

        boolean Right_first_highest_algorithm = true; // I
        boolean ride_before_decide = true;
        ////////////////////


        // initialize
        // TODO Set highest High
        createField(highest);

        // TODO Set SafeToEnd algorithm properties
        Algorithm algorithm = new SafeToEnd(false, Look_around_highest,false, false, rather_highest_then_end, visit_unvisited_around_highest_first, Right_first);
        // TODO Set highestBlockAlg algorithm properties
        Algorithm highestBlockAlg = new FindHighest(true, ride_before_decide, Right_first_highest_algorithm, false);

        // Start Algorithm
        System.out.println("Press button to start");
        if (!IS_VIRTUAL) Sound.twoBeeps();
        waitForKey();
        algorithm.start();

        // Start highest block finding algorithm
        if (!IS_VIRTUAL) Sound.twoBeeps();
//        if (BUTTON_STEPPING_DEBUG) {
//            System.out.println("Press button to search highest");
//            waitForKey();
//        }
        highestBlockAlg.start();
    }




    private static Field getTestField() {
        Field field = new Field(6, 9);

//        field.field[7][1].setMaybeLower().setHeight(1);

//        field.field[7][2].setMaybeLower().setHeight(1);

        field.field[7][3].setExact().setHeight(1);
        field.field[6][3].setExact().setHeight(2);
//        field.field[5][3].setExact().setHeight(1);

//        field.field[8][4].setExact().setHeight(0);
//        field.field[7][4].setMaybeLower().setHeight(1);
        field.field[6][4].setMaybeHigher().setHeight(3);
//        field.field[5][4].setMaybeHigher().setHeight(2);
//        field.field[4][4].setMaybeHigher().setHeight(1);
//        field.field[3][4].setMaybeLower().setHeight(1);
//        field.field[2][4].setMaybeHigher().setHeight(1);
//        field.field[1][4].setExact().setHeight(0);

//        field.field[8][5].setExact().setHeight(0);
//        field.field[7][5].setMaybeHigher().setHeight(2);
//        field.field[3][5].setMaybeLower().setHeight(1);
//        field.field[1][5].setExact().setHeight(0);


//        field.field[8][6].setMaybeHigher().setHeight(1);
//        field.field[3][6].setMaybeLower().setHeight(1);
//        field.field[1][6].setExact().setHeight(0);

        return field;
    }

}
