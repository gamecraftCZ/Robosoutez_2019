package cz.pusici;

import lejos.nxt.MotorPort;
import lejos.nxt.SensorPort;


// You can't use objects in config,
// because when you use them in code they get created and
// it causes VirtualField to fail with exception.
public class Config {

    public static final boolean IS_VIRTUAL = false;               // TODO IMPORTANT change to false
    public static final boolean IS_DEBUG = false;                 // TODO IMPORTANT change to false
    public static final boolean BUTTON_STEPPING_DEBUG = false;    // TODO IMPORTANT change to false


    ////////// Low Level config \\\\\\\\\\
    public static final MotorPort RIGHT_MOTOR_PORT = MotorPort.A;
    public static final MotorPort LEFT_MOTOR_PORT = MotorPort.B;
    public static final MotorPort BACK_MOTOR_PORT = MotorPort.C;

    public static final SensorPort FRONT_LIGHT_SENSOR_PORT = SensorPort.S2;
    public static final SensorPort FRONT_TOUCH_SENSOR_PORT = SensorPort.S1;
    public static final SensorPort GYRO_SENSOR_PORT = SensorPort.S4;
    public static final SensorPort ULTRASONIC_SENSOR_PORT = SensorPort.S3;

//    public static final MotorPort RIGHT_MOTOR_PORT = null;
//    public static final MotorPort LEFT_MOTOR_PORT = null;
//    public static final MotorPort BACK_MOTOR_PORT = null;
//
//    public static final SensorPort FRONT_LIGHT_SENSOR_PORT = null;
//    public static final SensorPort FRONT_TOUCH_SENSOR_PORT = null;
//    public static final SensorPort GYRO_SENSOR_PORT = null;
//    public static final SensorPort ULTRASONIC_SENSOR_PORT = null;

    public static final int MOVEMENT_SPEED = 600;
    public static final int FALL_SPEED = 300;
    public static final int CLIMB_SPEED = 300;

    public static final int ROTATION_SPEED = 100;
    public static final int BACK_MOTOR_SPEED = 900;

    public static final int BACK_MOTOR_DOWN_ROTATION = -800;  // In degrees

    public static final int ROTATION_90_DEGREES = 422;

    public static final int FRONT_LIGHT_THRESHOLD = 22;

    ////////// High Level config \\\\\\\\\\
    // Degrees of motor rotation to ride 1 block
    public static final int BLOCK_MOTOR_ROTATIONS = 680;
    public static final int LITTLE_FORWARD = 100;

}
